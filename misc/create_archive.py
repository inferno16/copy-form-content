import json
import zipfile
import os, sys

def get_manifest(main_dir):
    manifest = os.path.abspath(os.path.join(main_dir, 'manifest.json'))
    try:
        with open(manifest, 'r') as file_handle :
            data = json.load(file_handle)
        return data
    except:
        print('Can\'t read file "' + manifest + '"')
        sys.exit()

def check_version(main_dir, curr_dir, version):
    # Checking if there is tag with that name
    if(not os.path.exists(os.path.join(main_dir, '.git', 'refs', 'tags', 'v'+version))):
        print('Tag v' + version + ' is not created! Aborting...')
        sys.exit()
    
    # Checking if there is a archive with that name
    if(os.path.exists(os.path.join(curr_dir, 'v' + version + '.zip'))):
        print('An archive named v' + version + '.zip already exists! Aborting...')
        sys.exit()

def archive_dir(main_dir, curr_dir, version):
    check_version(main_dir, curr_dir, version)

    zf = zipfile.ZipFile('v' + version + '.zip', 'w')
    
    for dirname, subdirs, files in os.walk(main_dir):
        if(dirname == main_dir):
            # Remove the files and folders that we dont need
            subdirs.remove('.git')
            subdirs.remove('misc')
            files.remove('README.md')
            files.remove('.gitignore')
        for filename in files:
            absname = os.path.join(dirname, filename)
            arcname = absname[len(main_dir) + 1:] # This prevents creating folders from the absolute path
            if(arcname.endswith('.lnk')): # Don't add shortcut files
                continue
            zf.write(absname, arcname)
    zf.close()

if __name__ == '__main__':
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    # The main dir should be the parent directory
    main_dir = os.path.abspath(os.path.join(curr_dir, '..'))

    app = get_manifest(main_dir)
    archive_dir(main_dir, curr_dir, app['version'])
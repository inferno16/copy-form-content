(() => {
    const getFormContent = () => {
        const form = document?.activeElement?.closest('form');
        if (!form) {
            return;
        }
    
        return Array.from(form.elements ?? []).reduce(
            (elementsObj, element) => {
                const name = element.name.toLowerCase();
                const inputType = element.type.toLowerCase();
                const tagName = element.tagName.toLowerCase();
                if (
                    !name
                    || !['input', 'select', 'textarea'].includes(tagName)
                    || ['submit', 'button', 'reset', 'hidden'].includes(inputType)
                    || (tagName === 'input' && inputType === 'radio' && !element.checked)
                ) {
                    return elementsObj;
                }
    
                const type = tagName !== 'input' ? tagName : inputType;
                if (!elementsObj.hasOwnProperty(type)) {
                    elementsObj[type] = {};
                }
    
                let value;
                switch (type) {
                    case 'radio':
                        value = {value: element.value.toLowerCase(), id: element.id.toLowerCase()};
                        break;
                    case 'checkbox':
                        value = !!element.checked
                        break;
                    case 'select':
                        if (element.multiple) {
                            value = Array.from(element.querySelectorAll('option:checked')).map(el => el.value)
                            break;
                        }
                    default:
                        value = element.value;
                }
    
                elementsObj[type][name] = value;
    
                return elementsObj
            }, 
            {}
        );
    }
    
    const setFormData = (data) => {
        const form = document?.activeElement?.closest('form');
        if (!form) {
            return 0;
        }
        let elementsFilled = 0;
    
        Array.from(form.elements ?? []).forEach((element) => {
            const name = element.name.toLowerCase();
            const inputType = element.type.toLowerCase();
            const tagName = element.tagName.toLowerCase();
            const type = tagName !== 'input' ? tagName : inputType;
            if (!data[type]?.hasOwnProperty(name)) {
                return;
            }
            
            const value = data[type][name];
            switch (type) {
                case 'radio':
                    if (element.value.toLowerCase() === value.value || element.id.toLowerCase() === value.id) {
                        element.checked = true;
                        elementsFilled++;
                    }
                    break;
                case 'checkbox':
                    element.checked = value
                    elementsFilled++;
                    break;
                case 'select':
                    if (element.multiple) {
                        Array.from(element.options ?? []).forEach((option) => option.selected = value.includes(option.value))
                        elementsFilled++;
                        break;
                    }
                default:
                    element.value = value;
                    elementsFilled++
            }
        });

        return elementsFilled
    }

    const focusChanged = () => {
        const flag = !!document?.activeElement?.closest('form');
        try {
            chrome.runtime.sendMessage({ 
                type: 'updateContextMenu', 
                id: 'FormCopyParent', 
                options: {enabled: flag} 
            });
        } catch (error) {}
    };

    chrome.runtime.onMessage.addListener((message, _sender, response) => {
        let data;
        switch (message.type) {
            case 'copyForm':
                data = getFormContent();
                break;
            case 'pasteForm':
                data = setFormData(message.data);
                break;
        }

        response(data);
    });

    window.addEventListener('blur', focusChanged, true);
    window.addEventListener('focus', focusChanged, true);
})()
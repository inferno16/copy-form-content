const init = () => {
    const createSubMenu = () => {
        if (chrome.runtime.lastError) {
            // Context menu already created
            return;
        }

        chrome.contextMenus.create({
            id: 'FormCopyActionCopy',
            parentId: 'FormCopyParent',
            contexts: ['all'],
            title: 'Copy',
        });
        
        chrome.storage.session.get(['formData']).then((result) => {
            chrome.contextMenus.create({
                id: 'FormCopyActionPaste',
                parentId: 'FormCopyParent',
                contexts: ['all'],
                title: 'Paste',
                enabled: !!result.formData, // Enabled if there is form data stored in session
            });
        });
    };

    // Create context menu and attach it's submenu elements
    chrome.contextMenus.create(
        {
            id: 'FormCopyParent',
            title: 'Form Actions',
            contexts: ['all'],
            enabled: false,
        },
        createSubMenu
    );

    chrome.contextMenus.onClicked.addListener(contextMenuHandler);
    chrome.runtime.onMessage.addListener(messageHandler);
};

const messageHandler = (message) => {
    switch (message.type) {
        case 'updateContextMenu':
            chrome.contextMenus.update(message.id, message.options);
            break;
    }
};

const sendCopyMessage = (tabId) => {
    chrome.tabs.sendMessage(tabId, {type: 'copyForm'}, (response) => {
        if (!response) {
            return;
        }

        chrome.storage.session.set({formData: response}).then(() => {
            chrome.contextMenus.update('FormCopyActionPaste', {enabled: true});
        });
    });
};

const sendPasteMessage = (tabId) => {
    chrome.storage.session.get(['formData']).then((result) => {
        const formData = result.formData;
        chrome.tabs.sendMessage(tabId, {type: 'pasteForm', data: formData}, (response) => { 
            const elementsCount = Object.keys(formData).reduce((count, key) => count += Object.keys(formData[key]).length, 0);
            console.log(`${response}/${elementsCount} elements updated`) 
        });
    });
};

const contextMenuHandler = (e) => {
    try {
        chrome.tabs.query(
            { active: true, lastFocusedWindow: true }, 
            (tabs) => {
                const tabId = tabs[0]?.id;
                switch(e.menuItemId) {
                    case 'FormCopyActionCopy':
                        sendCopyMessage(tabId)
                        break;
                    case 'FormCopyActionPaste':
                        sendPasteMessage(tabId);
                        break;
                    default:
                        throw new Error('Unimplemented menu action: ' + e.menuItemId);
                }
            }
        );
        
    } catch (error) {
        console.log(error);
    }
};

init();